jQuery(document).ready(function () {

  $('.modal-open').modal('show');

  $.each($('.slider-brands'), function () {
    $(this).slick({
      slidesToScroll: 1,
      slidesToShow: 5,
      arrows: true,
      infinite: true,
      swipeToSlide: true,
      dots: false,
      prevArrow: '<a href="#" class="prev-button"></a>',
      nextArrow: '<a href="#" class="next-button"></a>',
      appendArrows: $(this).parents('.slider__wrap').find('.container-arrows'),
      // responsive: [
      //   {
      //     breakpoint: 991,
      //     settings: {
      //       slidesToShow: 2,
      //     }
      //   },
      //   {
      //     breakpoint: 768,
      //     settings: {
      //       slidesToShow: 1,
      //
      //     }
      //   }
      // ]

    });
  });
  
  
  $.each($('.slider-products'), function () {
    $(this).slick({
      slidesToScroll: 1,
      slidesToShow: 4,
      arrows: true,
      infinite: true,
      swipeToSlide: true,
      dots: false,
      prevArrow: '<a href="#" class="prev-button"></a>',
      nextArrow: '<a href="#" class="next-button"></a>',
      appendArrows: $(this).parents('.slider__wrap').find('.container-arrows'),
      // responsive: [
      //   {
      //     breakpoint: 991,
      //     settings: {
      //       slidesToShow: 2,
      //     }
      //   },
      //   {
      //     breakpoint: 768,
      //     settings: {
      //       slidesToShow: 1,
      //
      //     }
      //   }
      // ]

    });
  });


  $(".banner-slider").slick({
    infinity: true,
    fade:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrow: false,
    // swipeToSlide: false,
    asNavFor: '.banner-slider-nav',
  });


  $('.banner-slider-nav').slick({
    slidesToScroll: 1,
    slidesToShow: 7,
    asNavFor: '.banner-slider',
    infinite: false,
    focusOnSelect: true,
    arrows: false
  });
  
  
  $(".product-detail__slider").slick({
    infinity: true,
    fade:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrow: false,
    // swipeToSlide: false,
    asNavFor: '.product-detail__slider-nav',
  });

  
  $('.product-detail__slider-nav').slick({
    infinity: true,
    slidesToScroll: 1,
    slidesToShow: 3,
    asNavFor: '.product-detail__slider',
    focusOnSelect: true,
    arrows: true,
    prevArrow: '<a href="#" class="prev-button"></a>',
    nextArrow: '<a href="#" class="next-button"></a>',
    appendArrows: $('.product-detail__slider-nav').parents('.slider__wrap').find('.container-arrows'),

  });
  
  

  $('.accord-title--js').on('click', function () {
    var parents = $(this).parents('.accord-wrap--js');
    parents.find('.accord-block--js').slideToggle();
    parents.toggleClass('active');

  });

 /// formstyler

  $('.input-checkbox').styler()
  
 ///

  $('.item-active-js').on('click', function () {
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
  });


  $('.video-youtube').magnificPopup({
    type: 'iframe',
    closeOnContentClick: true,
    mainClass: 'mfp-img-mobile',
    image: {
      verticalFit: true
    }

  });


  $('.menu-item-js').mouseover(function() {
    var item = $(this).attr('data-menu-item'),
      menuBlock = $('.menu__block-js[data-menu-item= \'' + item + '\']');
    $(this).addClass('menu-open').siblings().removeClass('menu-open');
    menuBlock.addClass('menu-open').siblings().removeClass('menu-open');
  });
  
  $('.drop__menu').mouseleave(function(){
    $('.menu__block-js').removeClass('menu-open')
  })

  $('.button-catalog').on('click', function () {
    $(this).toggleClass('active');
    $('.drop__menu-wrap').toggleClass('active');
  });



  $(".icon-list").on('click', function () {
    $('.catalog__block').addClass('list');
  });

  $(".icon-tiles").on('click', function () {
    $('.catalog__block').removeClass('list');
  });
  
  
  
  /// range

  var $range = $('.js-range-slider'),
    $inputFrom = $(".js-input-from"),
    $inputTo = $(".js-input-to"),
    instance,
    min = 0,
    max = 1000,
    from = 0,
    to = 0;

  $range.ionRangeSlider({
    skin: "round",
    type: "double",
    min: min,
    max: max,
    from: 200,
    to: 800,
    onStart: updateInputs,
    onChange: updateInputs
  });
  instance = $range.data("ionRangeSlider");

  function updateInputs (data) {
    from = data.from;
    to = data.to;

    $inputFrom.prop("value", from);
    $inputTo.prop("value", to);
  }

  $inputFrom.on("input", function () {
    var val = $(this).prop("value");

    // validate
    if (val < min) {
      val = min;
    } else if (val > to) {
      val = to;
    }

    instance.update({
      from: val
    });
  });

  $inputTo.on("input", function () {
    var val = $(this).prop("value");

    // validate
    if (val < from) {
      val = from;
    } else if (val > max) {
      val = max;
    }

    instance.update({
      to: val
    });
  });

  $("#dropzone").dropzone({ url: "/file/post" });





//
//   $('.item-active-js').on('click', function () {
//     $(this).addClass('active');
//     $(this).siblings().removeClass('active');
//   });
//
//   $('.item-active-multi-js').on('click', function () {
//     $(this).toggleClass('active');
//   });
//
//   $(".tabs-item--js").on('click', function () {
//     var itemThumbs = $(this).attr('data-thumb'),
//       imgThumbs = $(".tabs__block--js[data-thumb= '" + itemThumbs + "']");
//     $(this).addClass('active').siblings().removeClass('active');
//     imgThumbs.addClass('active').siblings().removeClass('active');
//   });
//
//   //   /// mask
//   $(function () {
//     $(".phone").mask("+7 (999) 999 - 99 - 99");
//   });
//
//   //// count
//
//   $('.count-minus, .count-plus').click(function () {
//     var $input = $(this).parent().find('.input-count');
//     if ($(this).hasClass("count-minus")) {
//       var count = parseInt($input.val(), 10) - 1;
//     } else {
//       var count = parseInt($input.val(), 10) + 1;
//     }
//     count = count < 1 ? 1 : count;
//     $input.val(count);
//     $input.change();
//     return false;
//   });
//
//
//   $.each($('.slider-top'), function () {
//     $(this).slick({
//       slidesToScroll: 1,
//       slidesToShow: 1,
//       arrows: true,
//       infinite: true,
//       swipeToSlide: true,
//       dots: true,
//       fade: true
//     });
//   });
//
//   $.each($('.slider-product'), function () {
//     $(this).slick({
//       slidesToScroll: 1,
//       slidesToShow: 3,
//       arrows: true,
//       infinite: true,
//       swipeToSlide: true,
//       dots: false,
//       prevArrow: '<a href="#" class="prev-button button-circle"></a>',
//       nextArrow: '<a href="#" class="next-button button-circle"></a>',
//       appendArrows: $(this).parents('.slider__wrap').find('.container-arrows'),
//       responsive: [
//         {
//           breakpoint: 991,
//           settings: {
//             slidesToShow: 2,
//           }
//         },
//         {
//           breakpoint: 768,
//           settings: {
//             slidesToShow: 1,
//
//           }
//         }
//       ]
//
//     });
//   });
//
//
//   $("body").on('click', '.link-animate--js', function (e) {
//     var fixed_offset = 140;
//     $('html,body').stop().animate({scrollTop: $(this.hash).offset().top - fixed_offset}, 1000);
//     e.preventDefault();
//   });
//
//
//   $('.menu-item-js').on('click', function () {
//     var item = $(this).attr('data-menu-item'),
//       menuBlock = $('.menu__block-js[data-menu-item= \'' + item + '\']');
//     $(this).toggleClass('menu-open').siblings().removeClass('menu-open');
//     menuBlock.toggleClass('menu-open').siblings().removeClass('menu-open');
//     if (menuBlock.hasClass('menu-open')) {
//       $('.body').addClass('active');
//     } else {
//       $('.body').removeClass('active');
//
//
//     }
//
//   });
//
//
//   $('.accord-title--js').on('click', function () {
//     var parents = $(this).parents('.accord-wrap--js');
//     parents.find('.accord-block--js').slideToggle();
//     parents.toggleClass('active');
//
//   });
//
//   /// formstyler
//
//   $('.login-checkbox, .select, .select-sort').styler()
//
//   ///
//
//   $('.video-youtube').magnificPopup({
//     type: 'iframe',
//     closeOnContentClick: true,
//     mainClass: 'mfp-img-mobile',
//     image: {
//       verticalFit: true
//     }
//
//   });
//
//
//   // menu-catalog
//   $('.menu-header__item_catalog').hover(function (e) {
//     var item = $(this); // тут указываем ID элемента
//     var menu = $(".menu-dropdown_catalog"); // тут указываем ID элемента
//     menu.addClass('menu-open');
//     if (!item.is(e.target) // если клик был не по нашему блоку
//       && menu.is(e.target)) { // и не по его дочерним элементам
//       // item.removeClass('menu-open');
//       menu.removeClass('menu-open');
//     }
//     $('.body').addClass('active');
//   });
//
//
//   $('.menu-dropdown_catalog').mouseleave(function() {
//     $(this).removeClass('menu-open');
//     $('.body').removeClass('active');
//
//   });
//
//
//
//
//   // edit
//
//   $('.button-edit').on('click', function () {
//     var parent = $(this).parents('.input-row'),
//       input = parent.find('.input-item');
//     if ($(this).hasClass('active')) {
//       input.prop('disabled', false);
//     } else {
//       input.prop('disabled', true);
//     }
//   });
//
//
//
//   $('.humb').on('click', function () {
//     $('body').toggleClass('active');
//   });
//
//   var mql = window.matchMedia('(max-width: 991px)');
//
//   function screenTest(e) {
//     if (e.matches) {
//       $.each($('.partners-slider'), function () {
//         $(this).slick({
//           slidesToScroll: 1,
//           slidesToShow: 1,
//           arrows: true,
//           infinite: true,
//           swipeToSlide: true,
//           dots: false,
//           prevArrow: '<a href="#" class="prev-button button-circle"></a>',
//           nextArrow: '<a href="#" class="next-button button-circle"></a>',
//           appendArrows: $(this).parents('.slider__wrap').find('.container-arrows')
//         });
//       });
//
//       $('.catalog__block-title').on('click', function () {
//         var parents = $(this).parents('.catalog__block');
//         parents.find('.catalog__block-inner').slideToggle();
//         parents.toggleClass('active');
//
//       });
//      
//     }
//   }
//
//   mql.addListener(screenTest);
//
//   screenTest(mql)
//
//
//   $('.modal-open').modal('show');
//
//
//
//   $('.button__share, .share').on('click', function () {
//     $(this).find('.share-block').toggleClass('active');
//   });
//  
//   $('.filter-icon').on('click', function () {
//     $('.filter__wrap').slideToggle();
//   });
//  
//  
  
  
});





